var antlr4 = require('antlr4/index');
var HandlebarsLexer = require('HandlebarsLexer').HandlebarsLexer;
var HandlebarsParser = require('HandlebarsParser').HandlebarsParser;
var HandlebarsParserListener = require('HandlebarsParserListener').HandlebarsParserListener;

var eachFunction = function(ctx, body, list)
{
    var result = "";
    for (var i =0; i < list.length; i++)
    {
        result += body(list[i]);
    }
    return result;
}

var ifFunction = function(ctx, body, value)
{
    if (value)
        return body(ctx);

    return "";
}

var withFunction = function(ctx, body, name)
{
    var value = ctx[name];
    return body(value);
}

function HandlebarsCompiler() {
    HandlebarsParserListener.call(this);
    this._inputVar = "__$ctx";
    this._outputVar = "__$result";
    this._helpers = { expr: {}, block: {} };
    this._usedHelpers = {};
    this._bodyStack = [];
    this.registerBlockHelper("each", eachFunction);
    this.registerBlockHelper("if", ifFunction);
    this.registerBlockHelper("with", withFunction);
    return this;
}

HandlebarsCompiler.prototype = Object.create(HandlebarsParserListener.prototype);
HandlebarsCompiler.prototype.constructor = HandlebarsCompiler;

HandlebarsCompiler.escape = function (string) {
    return ('' + string).replace(/["'\\\n\r\u2028\u2029]/g, function (c) {
        switch (c) {
            case '"':
            case "'":
            case '\\':
                return '\\' + c;
            case '\n':
                return '\\n';
            case '\r':
                return '\\r';
            case '\u2028':
                return '\\u2028';
            case '\u2029':
                return '\\u2029';
        }
    })
};

HandlebarsCompiler.prototype.registerExprHelper = function(name, helper) {
    this._helpers.expr[name] = helper;
};

HandlebarsCompiler.prototype.registerBlockHelper = function (name, helper) {
    this._helpers.block[name] = helper;
};

function mangle(name)
{
    return "___" + name;
}

HandlebarsCompiler.prototype.compile = function (template) {
    this._usedHelpers = {};
    this._bodyStack = [];

    this.pushScope();

    var chars = new antlr4.InputStream(template);
    var lexer = new HandlebarsLexer(chars);
    var tokens = new antlr4.CommonTokenStream(lexer);
    var parser = new HandlebarsParser(tokens);
    parser.buildParseTrees = true;
    var tree = parser.document();
    antlr4.tree.ParseTreeWalker.DEFAULT.walk(this, tree);

    for (var name in this._usedHelpers)
    {
        var fn = this._helpers.expr[name] || this._helpers.block[name];
        var newName = mangle(name);
        var lambdaTemplate = `var ${newName} = ${fn.toString()}\n`;
        this._bodyStack[this._bodyStack.length - 1] = lambdaTemplate + this._bodyStack[this._bodyStack.length - 1];
    }

    var r = this.popScope()
    return new Function(this._inputVar, r);
};

HandlebarsCompiler.prototype.append = function (expr) {
    this._bodyStack[this._bodyStack.length - 1] += `${this._outputVar} += ${expr};\n`
};

HandlebarsCompiler.prototype.exitRawElement = function (ctx) {
    this.append(`"${HandlebarsCompiler.escape(ctx.getText())}"`);
};

HandlebarsCompiler.prototype.exitDataLookup = function(ctx)
{
    ctx.source = `${this._inputVar}.${ctx.getText()}`;
};

HandlebarsCompiler.prototype.exitLiteralExpr = function(ctx)
{
    ctx.source = ctx.getText();
}

HandlebarsCompiler.prototype.exitHelperApp = function(ctx)
{

    this._usedHelpers[ctx.name.text] = "yes";
    var mangled = mangle(ctx.name.text);
    var text = `${mangled}(${this._inputVar}`;
    for (var i = 0; i < ctx.params.length; i++)
    {
        text = text + ",";
        text = text + ctx.params[i].source;
        // if (i < ctx.params.length - 1)
        // {
        //     text = text + ",";
        // }

    }
    text = text + ")";
    ctx.source = text;
};

HandlebarsCompiler.prototype.exitValidExpressionElement = function(ctx)
{
    ctx.source = "";
    for (var i = 0; i < ctx.children.length; i++)
    {
        ctx.source += (ctx.children[i].source);
    }
};

HandlebarsCompiler.prototype.exitExpressionElement = function(ctx)
{
  if (ctx.children[1].source)
  {
      this.append(ctx.children[1].source);
  }
};

HandlebarsCompiler.prototype.exitParenthesizedExpr = function(ctx)
{
    ctx.source = ctx.children[1].source;
}

HandlebarsCompiler.prototype.pushScope = function()
{
    this._bodyStack.push("");
    this._bodyStack[this._bodyStack.length - 1] = `var ${this._outputVar} = "";\n`;
}

HandlebarsCompiler.prototype.popScope = function()
{
    this._bodyStack[this._bodyStack.length - 1] += `return ${this._outputVar};\n`;
    return this._bodyStack.pop();

}

HandlebarsCompiler.prototype.enterBlockBody = function(ctx)
{
    this.pushScope();
}

HandlebarsCompiler.prototype.exitBlockBody = function(ctx)
{
    ctx.source = (new Function(this._inputVar, this.popScope())).toString();
}

HandlebarsCompiler.prototype.exitBlockElement = function(ctx)
{
    if (ctx.start.app.name.text != ctx.end.name.text) {
        throw `Block start '${ctx.start.app.name.text}' does not match the block end '${ctx.end.name.text}'.`;
    }

    this._usedHelpers[ctx.start.app.name.text] = "yes";
    var mangled = mangle(ctx.start.app.name.text);
    var text = `${mangled}(${this._inputVar},${ctx.body.source}`;
    for (var i = 0; i < ctx.start.app.params.length; i++)
    {
        text = text + ",";
        text = text + ctx.start.app.params[i].source;
        // if (i < ctx.params.length - 1)
        // {
        //     text = text + ",";
        // }

    }
    text = text + ")";
    this.append(text);
}

exports.HandlebarsCompiler = HandlebarsCompiler;
