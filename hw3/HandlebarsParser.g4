parser grammar HandlebarsParser;

options { language=JavaScript; tokenVocab=HandlebarsLexer; }

document : element* EOF ;

element
    : rawElement
    | expressionElement
    | blockElement
    | commentElement
    ;

rawElement  : TEXT
 | TEXT skippedBraces;

skippedBraces : '{'+;

commentElement : START COMMENT END_COMMENT ;

expressionElement    :  START validExpressionElement END ;

validExpressionElement returns [source]:
    dataLookup
    | literalExpr
    | parenthesizedExpr
    | helperApp;

literalExpr returns [source]:
    stringLiteralExpr
    | floatLiteralExpr
    | intLiteralExpr;

stringLiteralExpr: STRING;
floatLiteralExpr: FLOAT;
intLiteralExpr: INTEGER;

helperApp returns [source] : name=ID (params+=validExpressionElement)+;
dataLookup returns [source] : ID;
parenthesizedExpr returns [source] : OPEN_PAREN validExpressionElement CLOSE_PAREN;

blockElement:
    start=blockOpening body=blockBody end=blockClosing;

blockOpening: START BLOCK app=helperApp END;
blockBody returns [source]: element*;
blockClosing: START CLOSE_BLOCK name=ID END;