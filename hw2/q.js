/* Q is a small query language for JavaScript.
 *
 * Q supports simple queries over a JSON-style data structure,
 * kinda like the functional version of LINQ in C#.
 *
 * ©2016, Pavel Panchekha and the University of Washington.
 * Released under the MIT license.
 */

//// The AST

// This class represents all AST Nodes.
function ASTNode(type) {
    this.type = type;
}

ASTNode.prototype = {};

// The All node just outputs all records.
function AllNode() {
    ASTNode.call(this, "All");
}

// This is how we make AllNode subclass from ASTNode.
AllNode.prototype = Object.create(ASTNode.prototype);


// The Filter node uses a callback to throw out some records.
function FilterNode(callback) {
    ASTNode.call(this, "Filter");
    this.callback = callback;
}

FilterNode.prototype = Object.create(ASTNode.prototype);

// The Then node chains multiple actions on one data structure.
function ThenNode(first, second) {
    ASTNode.call(this, "Then");
    this.first = first;
    this.second = second;
}

ThenNode.prototype = Object.create(ASTNode.prototype);




//// Executing queries

ASTNode.prototype.execute = function (table) {
    throw new Error("Unimplemented AST node " + this.type)
}

// ...
AllNode.prototype.execute = function (table) {
    return table.slice();
}

ThenNode.prototype.execute = function (table) {
    var f = this.first.execute(table);
    return this.second.execute(f);
}

FilterNode.prototype.execute = function (table) {
    var arr = [];
    for (var i = 0; i < table.length; i++) {
        var element = table[i];
        if (this.callback(element)) {
            arr.push(element);
        }
    }
    return arr;
}



//// Write a query
// Define the `thefts_query` and `auto_thefts_query` variables

var thefts_query = new FilterNode(function (node) { return node[13].match(/THEFT/); });

var auto_thefts_query = new FilterNode(function (node) { return node[13].match(/^VEH-THEFT/); });



//// Add Apply and Count nodes

// ...
// The apply node is equivalent to `map`
function ApplyNode(callback) {
    ASTNode.call(this, "Apply");
    this.callback = callback;
}

ApplyNode.prototype = Object.create(ASTNode.prototype);

ApplyNode.prototype.execute = function (table) {
    var result = [];
    for (var i = 0; i < table.length; i++) {
        result.push(this.callback(table[i]));
    }
    return result;
}

// The count node returns the number of elements in the table as a single element array
function CountNode() {
    ASTNode.call(this, "Count");
}

CountNode.prototype = Object.create(ASTNode.prototype);

CountNode.prototype.execute = function (table) {
    return [table.length];
}

//// Clean the data

var cleanupCallback = function (element) {
    var type = element[13];
    var description = element[15]
    var date = element[17];
    var loc = element[18];
    return { type: type, description: description, date: date, area: loc };
}

var cleanup_query = new ApplyNode(cleanupCallback);


//// Implement a call-chaining interface

// ...
Q = Object.create(new AllNode());

ASTNode.prototype.filter = function (callback) {
    var filterNode = new FilterNode(callback);
    return new ThenNode(this, filterNode);
}

ASTNode.prototype.apply = function (callback) {
    var applyNode = new ApplyNode(callback);
    return new ThenNode(this, applyNode);
}

ASTNode.prototype.count = function () {
    return new ThenNode(this, new CountNode());
}





//// Reimplement queries with call-chaining

var cleanup_query_2 = Q.apply(cleanupCallback); // ...

var thefts_query_2 = Q.filter(function (node) { return node.type.match(/THEFT/); }); // ...

var auto_thefts_query_2 = Q.filter(function (node) { return node.type.match(/^VEH-THEFT/); }); // ...




//// Optimize filters

ASTNode.prototype.optimize = function () { return this; }

ThenNode.prototype.optimize = function () {
    return new ThenNode(this.first.optimize(), this.second.optimize())
}

// We add a "run" method that is like "execute" but optimizes queries first.

ASTNode.prototype.run = function (data) {
    this.optimize().execute(data);
}

function AddOptimization(node_type, f) {
    var old = node_type.prototype.optimize;
    node_type.prototype.optimize = function () {
        var new_this = old.apply(this, arguments);
        return f.apply(new_this, arguments) || new_this;
    }
}

// ...
// ThenNode(ThenNode(ThenNode(... , CartesianNode), FilterNode), ApplyNode)
AddOptimization(ThenNode, function () {
    if (this.type != 'Then')
        return;
    
    if (this.first.type == 'Then' && this.second.type == 'Filter' && this.first.second.type == 'Filter') {
        var firstFilter = this.first.second;
        var secondFilter = this.second;
        var newFilter = new FilterNode(firstFilter && secondFilter);
        return new ThenNode(this.first.first, newFilter);
    }
    if (this.second.type == 'Apply' &&
        this.first.type == 'Then' && 
         this.first.first.type == 'Then' &&
        this.first.second.type == 'Filter' &&
        this.first.first.second.type == 'CartesianProductNode')
    {   
        var jn = new JoinNode(this.first.second.callback, this.first.first.second.right, this.first.first.second.left);
        return jn.optimize() || jn;  
    }
});



//// Internal node types and CountIf

// ...

function CountIfNode(callback) {
    ASTNode.call(this, "CountIf");
    this.callback = callback
}

CountIfNode.prototype = Object.create(ASTNode.prototype);

CountIfNode.prototype.execute = function (table) {
    var result = 0;
    for (var i = 0; i < table.length; i++)
    {
        if (this.callback(table[i]))
            result += 1;
    }
    return result;
}

AddOptimization(ThenNode, function () {
    if (this.type == 'Then' && this.first.type == 'Then' && this.second.type == 'Count' && this.first.second.type == 'Filter') {
        var firstFilter = this.first.second;
        var newFilter = new CountIfNode(firstFilter.callback)
        return new ThenNode(this.first.first, newFilter);
    }
});

//// Cartesian Products

// ...

function CartesianProductNode(left, right)
{
    ASTNode.call(this, "CartesianProductNode");
    this.left = left;
    this.right = right;
}

CartesianProductNode.prototype = Object.create(ASTNode.prototype);

CartesianProductNode.prototype.execute = function(table)
{
    var first = this.left.execute(table);
    var second = this.right.execute(table);

    var result = [];
    for (var i = 0; i < first.length; i++)
    {
        for (var j = 0; j < second.length; j++)
        {
            var left = first[i];
            var right = second[j];
            result.push({ left: left, right: right });
        }
    }
    return result;
}

// CartesianProductNode.prototype.optimize = function()
// {
//     return new CartesianProductNode(this.left.optimize(), this.right.optimize());
// }

//// Joins

// ...

ASTNode.prototype.product = function (l, r) {
    return new ThenNode(this, new CartesianProductNode(l, r));
}


function projectFields2(left, right)
{
    var res = [];
    var i;
    for (i = 0; i < right.length; i++) {
        res.push(right[i]);
    }
    for (var k = i; k < left.length; k++) {
        res.push(left[k]);
    }
    return res;
}


function projectFields(element)
{
    var res = [];
    var i;
    for (i = 0; i < element.right.length; i++) {
        res.push(element.right[i]);
    }
    for (var k = i; k < element.left.length; k++) {
        res.push(element.left[k]);
    }
    return res;
}

// ThenNode(ThenNode(ThenNode(... , CartesianNode), FilterNode), ApplyNode)

ASTNode.prototype.join = function (f, l, r) {
    var productNode = new ThenNode(this, new CartesianProductNode(l, r));
    var filterFn = function(e) {return f(e.left, e.right)};
    filterFn.data = f.data;
    var filterNode = new ThenNode(productNode, new FilterNode(filterFn));
    var mapNode = new ThenNode(filterNode, new ApplyNode(projectFields));
    return mapNode;
}

//// Optimizing joins

// ...

function JoinNode(f, l, r)
{
    ASTNode.call(this, "Join");
    this.callback = f;
    this.left = l;
    this.right = r;
}

JoinNode.prototype = Object.create(ASTNode.prototype);

JoinNode.prototype.execute = function(table)
{
    var cart = new CartesianProductNode(this.left, this.right);
    var product = cart.execute(table);

    var result = []
    for (var i =0; i < product.length; i++)
    {
        var elem = product[i];
        if (callback(elem))
            result.push(projectFields(elem));

    }

    return result;
}

JoinNode.prototype.optimize = function()
{
    return new JoinNode(this.callback, this.left.optimize(), this.right.optimize());
}


//// Join on fields

// ...

ASTNode.prototype.on = function(field)
{
    var f = function(left, right) { return left[field] == right[field];  }
    f.data = {field: field};
    return f;
}


//// Implement hash joins

// ...

function HashJoinNode(field, left, right)
{
    ASTNode.call(this, "HashJoin");
    this.field = field;
    this.left = left;
    this.right = right;
}

HashJoinNode.prototype = Object.create(ASTNode.prototype);

function buildHashTable(node, field, table)
{
    var hash = {};
   var data = node.execute(table);
    for (var i = 0; i < data.length; i++)
    { 
        var elem = data[i];
        if (field in elem)
        {
            var key = elem[field]
            if (!(key in hash))
            {
                hash[key] = [];
            }
            
            hash[key].push(elem)
        }
    }
    
    return hash; 
}

HashJoinNode.prototype.execute = function(table)
{
    var leftHash = buildHashTable(this.left, this.field, table);
    var rightHash = buildHashTable(this.right, this.field, table);
    
    var result = [];
    var keys = Object.keys(leftHash);
    keys.forEach(function(key, index)
    {
        var leftSet = leftHash[key];
        var rightSet = rightHash[key] || [];
        
        for (var i = 0; i < leftSet.length; i++)
        {
            for (var j = 0; j < rightSet.length; j++)
            {
                result.push(projectFields2(leftSet[i], rightSet[j]));
            }
        }
    });
    
    return result;
}

HashJoinNode.prototype.optimize = function()
{
    return new HashJoinNode(this.field, this.left.optimize(), this.right.optimize());
}



//// Optimize joins on fields to hash joins

// ...
AddOptimization(JoinNode, function () {
    var joinFn = this.callback;
    if (('data' in joinFn) && (joinFn.data) && ('field' in joinFn.data))
    {
        var field = joinFn.data.field;
        return new HashJoinNode(field, this.left, this.right);
    }
})