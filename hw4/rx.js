
function Stream() {
    this.callbacks = []
}

Stream.prototype.subscribe = function(callback)
{
    this.callbacks.push(callback);
}

Stream.prototype._push = function(elem)
{
    for (var i = 0; i < this.callbacks.length; i++)
    {
        this.callbacks[i](elem);
    }
}

Stream.prototype._push_many = function(arr)
{
    for (var i = 0; i < arr.length; i++)
    {
        this._push(arr[i]);
    }
}

Stream.prototype.first = function()
{
    var antecedent = new Stream();
    var flag = true;
    var callback = function(elem)
    {
        if (flag)
        {
            flag = false;
            antecedent._push(elem);
        }
    }

    this.subscribe(callback);
    return antecedent;
}

Stream.prototype.map = function(selector)
{
    var antecedent = new Stream();
    this.subscribe(function(elem)
    {
        antecedent._push(selector(elem));
    })

    return antecedent;
}

Stream.prototype.filter = function(callback)
{
    var antecedent = new Stream();
    this.subscribe(function(elem)
    {
        if (callback(elem))
            antecedent._push(elem);
    })
    return antecedent;
}

Stream.prototype.flatten = function()
{
    var antecedent = new Stream();
    this.subscribe(function(elem)
    {
        antecedent._push_many(elem);
    })
    return antecedent;
}

Stream.prototype.join = function(other)
{
    var antecdent = new Stream();
    var callback = function(elem) { antecdent._push((elem)); };
    this.subscribe(callback);
    other.subscribe(callback);
    return antecdent;
}

Stream.prototype.combine = function()
{
    var antecedent = new Stream();
    var callback = function(elem) { antecedent._push((elem));};
    this.subscribe(function(stream) { stream.subscribe(callback) });
    return antecedent;

}

Stream.prototype.zip = function(other, selector)
{
    var antecedent = new Stream();

    var aVal;
    var bVal;
    var aReturned = false;
    var bReturned = false;

    var onA = function(elem)
    {
        aReturned = true;
        aVal = elem;
        if (bReturned)
        {
            antecedent._push(selector(aVal, bVal));
        }
    };

    var onB = function(elem)
    {
        bReturned = true;
        bVal = elem;
        if (aReturned)
        {
            antecedent._push(selector(aVal, bVal));
        }
    };

    this.subscribe(onA);
    other.subscribe(onB);

    return antecedent;
};

Stream.prototype.timer = function(n)
{
    var stream= new Stream();
    setInterval(function() {stream._push(new Date());}, n);
    return stream;
}

Stream.prototype.dom = function(element, eventname)
{
    var stream = new Stream();
    element.on(eventname, function(eventObj) { stream._push(eventObj); });
    return stream;

}

Stream.prototype.throttle = function(n)
{
    var stream = new Stream();
    var receivedVal;
    var hasReceived = false;
    this.subscribe(function(elem)
    {
        receivedVal = elem;
        hasReceived = true;
    })

    var timer = this.timer(n);
    timer.subscribe(function(_)
    {
        if (hasReceived)
        {
            stream._push(receivedVal);
            hasReceived = false;
        }
    })

    return stream;

};

Stream.prototype.url = function(url)
{
    var stream = new Stream();
    var cb = function(response)
    {
        stream._push(response);
    };
    $.get(url, cb, "json");
    return stream;
}

Stream.prototype.latest = function()
{
    var stream = new Stream();
    var usedStreams = [];
    var lastStream = null;
    this.subscribe(function(s)
    {
        if (lastStream != null && lastStream != s)
            usedStreams.push(lastStream);

        lastStream = s;
        s.subscribe(function(elem)
        {
            if (s == lastStream && $.inArray(s, usedStreams) == -1)
                stream._push(elem);
        })
    });

    return stream;

};

Stream.prototype.unique = function()
{
    var dict = {};
    var stream = new Stream();

    this.subscribe(function(elem)
    {
        if (!(elem in dict))
            stream._push(elem);
        dict[elem] = 0;

    })
    return stream;
}

Stream.prototype.fromArray = function(arr)
{
    var stream = new Stream();
    stream.start = function()
    {
        for (var i = 0; i < arr.length; i++)
            stream._push(arr[i]);
    }
    return stream;
}

// PART 1 HERE

var FIRE911URL = "https://data.seattle.gov/views/kzjm-xkqj/rows.json?accessType=WEBSITE&method=getByIds&asHashes=true&start=0&length=10&meta=false&$order=:id";

window.WIKICALLBACKS = {}

function WIKIPEDIAGET(s, cb) {
    $.ajax({
        url: "https://en.wikipedia.org/w/api.php",
        dataType: "jsonp",
        jsonp: "callback",
        data: {
            action: "opensearch",
            search: s,
            namespace: 0,
            limit: 10,
        },
        success: function(data) {cb(data[1])},
    })
}

$(function() {
    // PART 2 INTERACTIONS HERE
    var dateStream = new Stream().timer(20);
    dateStream.subscribe(function(elem) { $("#time").text(elem); })

    // Click count
    var clicks = $("#clicks");
    var button = $('#button');
    var clickStream = new Stream().dom(button, "click");
    var count = 0;
    clickStream.subscribe(function(e){
        count++;
        clicks.text(count);
    });

    var mousePositionSpan = $("#mouseposition");
    var mouseMoveDiv = $("#mousemove");
    var mouseMoveStream = new Stream().dom(mouseMoveDiv, "mousemove");
    var throttledMouseMove = mouseMoveStream.throttle(1000);
    throttledMouseMove.subscribe(function(e){
        mousePositionSpan.text("X:" + e.pageX + " Y: " + e.pageY);
    });

    // Fire stream part 1 (commented out)
    // var urlStream = new Stream().url(FIRE911URL).flatten();
    // urlStream.subscribe(function(elem) {
    //     var address = elem["3479077"];
    //     $("#fireevents").append($("<li></li>").text(address))
    // });

    var inputBox = $("#wikipediasearch");
    var inputStream = new Stream().dom(inputBox, "input")
        .throttle(100);

    var searchStreamStream = new Stream();

    inputStream.subscribe(function(e){
        var searchStream = new Stream();
        var searchTerm = inputBox[0].value;
        WIKIPEDIAGET(searchTerm, function(titles){
            searchStream._push(titles);
        });
        searchStreamStream._push(searchStream);
    });

    var titlesStream = searchStreamStream.latest();
    titlesStream.subscribe(function(titles){
        $("#wikipediasuggestions").empty();
        for(var i = 0; i < titles.length; i++){
            var title = titles[i];
            $("#wikipediasuggestions").append($("<li></li>").text(title));
        }
    });


    // Fire search part 2:
    var addresses = [];
    var fireSearchStream = new Stream();
    var timer = new Stream().timer(60000);
    timer.subscribe(function(_) { fireSearchStream._push(new Stream().url(FIRE911URL).flatten() ) });
    var latest = fireSearchStream.latest().map(function(elem)
    {
        return elem["3479077"]
    }).unique();
    latest.subscribe(function(elem) { addresses.push(elem)});
    latest.subscribe(function(_) {updateDisplay()});

    var searchBox = $("#firesearch");

    var updateDisplay = function()
    {
        $("#fireevents").empty();
        var valid = $.grep(addresses, function(a)
        {
            return a.indexOf(searchBox.val()) != -1;
        } );
        $.map(valid, function(e)
        {
            $("#fireevents").append($("<li></li>").text(e));
        } )
    }

    var domStream = new Stream().dom(searchBox, "input");
    domStream.subscribe(function(_) { updateDisplay(); });


    // Give the timer a kick to get things started
    timer._push(0);

    /* Food for thought:
        I use Jquery to map/filter when I update the UI because you always
        have to fully clear/rewrite the list, but I definitely could have
        done that using streams. The stream API is a little awkward
        because you have to start with one that generates events, you can't
        just make one out of a collection.

        But assuming I had dom().filter().map(update UI),
        the obvious optimization would be to add a filtermap and mapfilter node types.


     */



});
