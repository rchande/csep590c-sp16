﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSProjBuilder;
using static CSProjBuilder.ElementHelpers;

namespace NewProjectGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Usage: Generator.exe {console|library} [name]");
            }

            var project = GetProjectFile();
            string name = args[1];
            project = project.Substitute("$RootNamespace", name).Substitute("$AssemblyName", name);
            project = project.Substitute("$ProjectGuid", Guid.NewGuid().ToString());
            string type = args[0];
            if (type == "console")
            {
                project = ApplyConsoleSpecificTweaks(project);
            }
            else if (type == "Library")
            {
                project = ApplyLibrarySpecificTweaks(project);
            }
            else
            {
                Console.WriteLine("Please specify console app or library");
                return;
            }

            File.WriteAllText($"{name}.csproj", project.ToString());
        }

        static ProjFile GetProjectFile()
        {
            var proj = new ProjFile();
            proj = proj.WithToolsVersion(@"14.0");
            proj = proj.WithDefaultTargets(@"Build");
            proj = proj.WithInitialXMLns(@"http://schemas.microsoft.com/developer/msbuild/2003");
            proj = proj.WithConditionalImport(@"$(MSBuildExtensionsPath)\$(MSBuildToolsVersion)\Microsoft.Common.props", @"Exists('$(MSBuildExtensionsPath)\$(MSBuildToolsVersion)\Microsoft.Common.props')"); proj = proj.WithPropertyGroup()
             .WithConditionalProperty(@"Configuration", @"Debug", @" '$(Configuration)' == '' ")
             .WithConditionalProperty(@"Platform", @"AnyCPU", @" '$(Platform)' == '' ")
             .WithProperty(new ResolvedTemplateString("ProjectGuid"), new TemplateString("$ProjectGuid"))
             .WithProperty(new ResolvedTemplateString(@"OutputType"), new TemplateString("$OutputType"))
             .WithProperty(@"AppDesignerFolder", @"Properties")
             .WithProperty(new ResolvedTemplateString(@"RootNamespace"), new TemplateString("$RootNamespace"))
             .WithProperty(new ResolvedTemplateString(@"AssemblyName"), new TemplateString("$AssemblyName"))
             .WithProperty(@"TargetFrameworkVersion", @"v4.5.2")
             .WithProperty(@"FileAlignment", @"512")
             .WithProperty(@"AutoGenerateBindingRedirects", @"true")
             .EndPropertyGroup();
            proj = proj.WithPropertyGroup()
            .WithCondition($@" '$(Configuration)|$(Platform)' == 'Debug|AnyCPU' ")
            .WithProperty(@"PlatformTarget", @"AnyCPU")
            .WithProperty(@"DebugSymbols", @"true")
            .WithProperty(@"DebugType", @"full")
            .WithProperty(@"Optimize", @"false")
            .WithProperty(@"OutputPath", @"bin\Debug\")
            .WithProperty(@"DefineConstants", @"DEBUG;TRACE")
            .WithProperty(@"ErrorReport", @"prompt")
            .WithProperty(@"WarningLevel", @"4")
            .EndPropertyGroup();
            proj = proj.WithPropertyGroup()
            .WithCondition($@" '$(Configuration)|$(Platform)' == 'Release|AnyCPU' ")
            .WithProperty(@"PlatformTarget", @"AnyCPU")
            .WithProperty(@"DebugType", @"pdbonly")
            .WithProperty(@"Optimize", @"true")
            .WithProperty(@"OutputPath", @"bin\Release\")
            .WithProperty(@"DefineConstants", @"TRACE")
            .WithProperty(@"ErrorReport", @"prompt")
            .WithProperty(@"WarningLevel", @"4")
            .EndPropertyGroup();
            proj = proj.WithItemGroup()
            .WithItem(Reference("System"))
            .WithItem(Reference("System.Core"))
            .WithItem(Reference("System.Xml.Linq"))
            .WithItem(Reference("System.Data.DataSetExtensions"))
            .WithItem(Reference("Microsoft.CSharp"))
            .WithItem(Reference("System.Data"))
            .WithItem(Reference("System.Net.Http"))
            .WithItem(Reference("System.Xml"))
            .EndItemGroup();
            proj = proj.WithItemGroup()
            .WithItem(Compile("Program.cs"))
            .WithItem(Compile(@"Properties\AssemblyInfo.cs"))
            .EndItemGroup();

            return  proj.WithImport(@"$(MSBuildToolsPath)\Microsoft.CSharp.targets");
        }

        static ProjFile ApplyConsoleSpecificTweaks(ProjFile project)
        {
            return project.Substitute("$OutputType", "Exe")
                .WithItemGroup()
            .WithItem(None("App.config"))
            .EndItemGroup();
        }

        static ProjFile ApplyLibrarySpecificTweaks(ProjFile project)
        {
            return project.Substitute("$OutputType", "Library");
        }
    }
}
