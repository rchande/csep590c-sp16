﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSProjBuilder;
using static CSProjBuilder.ElementHelpers;

namespace TestProjectGenerator
{
    // The project file for our tests is really complicated, so we 
    // generate it from scratch when you enlist for the first time. 
    class Program
    {
        static void Main(string[] args)
        {
            var proj = new ProjFile();
            proj = proj.WithToolsVersion(@"14.0");
            proj = proj.WithDefaultTargets(@"Build");
            proj = proj.WithInitialXMLns(@"http://schemas.microsoft.com/developer/msbuild/2003");
            proj = proj.WithConditionalImport(@"$(MSBuildExtensionsPath)\$(MSBuildToolsVersion)\Microsoft.Common.props", @"Exists('$(MSBuildExtensionsPath)\$(MSBuildToolsVersion)\Microsoft.Common.props')"); proj = proj.WithPropertyGroup()
             .WithConditionalProperty(@"Configuration", @"Debug", @" '$(Configuration)' == '' ")
             .WithConditionalProperty(@"Platform", @"AnyCPU", @" '$(Platform)' == '' ")
             .WithProperty(@"ProjectGuid", @"{134C81EE-D9E3-46FF-A6E6-E8CE4501E0B9}")
             .WithProperty(@"OutputType", @"Exe")
             .WithProperty(@"AppDesignerFolder", @"Properties")
             .WithProperty(@"RootNamespace", @"ProjectConverter")
             .WithProperty(@"AssemblyName", @"ProjectConverter")
             .WithProperty(@"TargetFrameworkVersion", @"v4.5.2")
             .WithProperty(@"FileAlignment", @"512")
             .WithProperty(@"AutoGenerateBindingRedirects", @"true")
             .EndPropertyGroup();
            proj = proj.WithPropertyGroup()
            .WithCondition($@" '$(Configuration)|$(Platform)' == 'Debug|AnyCPU' ")
            .WithProperty(@"PlatformTarget", @"AnyCPU")
            .WithProperty(@"DebugSymbols", @"true")
            .WithProperty(@"DebugType", @"full")
            .WithProperty(@"Optimize", @"false")
            .WithProperty(@"OutputPath", @"bin\Debug\")
            .WithProperty(@"DefineConstants", @"DEBUG;TRACE")
            .WithProperty(@"ErrorReport", @"prompt")
            .WithProperty(@"WarningLevel", @"4")
            .EndPropertyGroup();
            proj = proj.WithPropertyGroup()
            .WithCondition($@" '$(Configuration)|$(Platform)' == 'Release|AnyCPU' ")
            .WithProperty(@"PlatformTarget", @"AnyCPU")
            .WithProperty(@"DebugType", @"pdbonly")
            .WithProperty(@"Optimize", @"true")
            .WithProperty(@"OutputPath", @"bin\Release\")
            .WithProperty(@"DefineConstants", @"TRACE")
            .WithProperty(@"ErrorReport", @"prompt")
            .WithProperty(@"WarningLevel", @"4")
            .EndPropertyGroup();
            proj = proj.WithItemGroup()
            .WithItem(Reference("System"))
            .WithItem(Reference("System.Core"))
            .WithItem(Reference("System.Xml.Linq"))
            .WithItem(Reference("System.Data.DataSetExtensions"))
            .WithItem(Reference("Microsoft.CSharp"))
            .WithItem(Reference("System.Data"))
            .WithItem(Reference("System.Net.Http"))
            .WithItem(Reference("System.Xml"))
            .EndItemGroup();
            proj = proj.WithItemGroup()
            .WithItem(Compile("Program.cs"))
            .WithItem(Compile(@"Properties\AssemblyInfo.cs"))
            .EndItemGroup();
            proj = proj.WithItemGroup()
            .WithItem(None("App.config"))
            .EndItemGroup();

            var projectReference = ElementHelpers.ProjectReference(@"..\CSProjBuilder\CSProjBuilder.csproj", "CSprojBuilder", "{c97e2b61-7b21-42e1-a00d-e8294464aea3}");

            proj = proj.WithItemGroup()
            .WithItem(projectReference)
            .EndItemGroup();

            proj = proj.WithImport(@"$(MSBuildToolsPath)\Microsoft.CSharp.targets");

            File.WriteAllText("CSProjBuilderTests.csproj", proj.ToString());
        }
    }
}
