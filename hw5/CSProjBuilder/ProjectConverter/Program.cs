﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSProjBuilder;

namespace ProjectConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Usage: ProjectConverter.exe [projectfile]");
                return;
            }

            var text = File.ReadAllText(args[0]);
            Console.WriteLine(Converter.ConvertCSProjToProgram(text));
        }
    }
}
