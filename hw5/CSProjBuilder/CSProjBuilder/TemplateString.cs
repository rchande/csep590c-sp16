﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSProjBuilder
{
    /// <summary>
    /// TemplateString is a placeholder indicating a string that can
    /// (or may already) have been replaced. When serializing, 
    /// elements call TemplateString.ToString(). The base TemplateString
    /// throws an exception to indicate that it hasn't been substituted.
    /// 
    /// When you ask an element to substitute a (key, value), it returns itself
    /// with all templates of key replaced with a ResolvedTemplateString(value).
    /// ResolvedTemplateString.ToString() simply returns the string. You may
    /// use ResolvedTemplateString if you want to use a Template overload without
    /// templating every string.
    /// </summary>
    public class TemplateString
    {
        public TemplateString(string name)
        {
            Name = name;
        }

        public string Name { get; }

        public override string ToString()
        {
            if (Name == null)
            {
                return "";
            }

            throw new Exception($"Found unsubstituted template {Name}");
        }

        public virtual TemplateString Substitute(string key, string value)
        {
            if (key == Name)
            {
                return new ResolvedTemplateString(value);
            }

            return this;
        }
    }

    public class ResolvedTemplateString : TemplateString
    {
        public ResolvedTemplateString(string name) : base(name) { }

        public override string ToString()
        {
            return Name;
        }

        public override TemplateString Substitute(string key, string value)
        {
            return this;
        }
    }
}
