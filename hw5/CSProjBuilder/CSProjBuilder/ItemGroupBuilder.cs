﻿using System;
using System.Collections.Immutable;

namespace CSProjBuilder
{
    /// <summary>
    /// Implementaiton of IItemGroupBuilder
    /// </summary>
    internal class ItemGroupBuilder : IItemGroupBuilder
    {
        private readonly ProjFile parent;
        private readonly ImmutableList<Element> elements;
        private readonly string condition;

        public ItemGroupBuilder(ProjFile projFile)
        {
            this.parent = projFile;
            elements = ImmutableList.Create<Element>();
        }

        private ItemGroupBuilder(ProjFile projFile, ImmutableList<Element> elements, string condition)
        {
            this.parent = projFile;
            this.elements = elements;
            this.condition = condition;
        }

        public ProjFile EndItemGroup()
        {
            return this.parent.WithItemGroup(this.ToItemGroup());
        }

        private ItemGroup ToItemGroup()
        {
            return new ItemGroup(elements);
        }

        public IItemGroupBuilder WithCondition(string condition)
        {
            return new ItemGroupBuilder(parent, elements, condition);
        }

        public IItemGroupBuilder WithItem(Element elem)
        {
            return new ItemGroupBuilder(parent, elements.Add(elem), condition);
        }
    }
}