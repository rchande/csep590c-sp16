﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CSProjBuilder
{
    /// <summary>
    /// Represents an MSBuild Import attribute. Supports templating. 
    /// </summary>
    public class Import : IAddToXml, ITemplate
    {
        public Import(string project, string condition) : this(new ResolvedTemplateString(project), new ResolvedTemplateString(condition))
        {
        }

        public Import(TemplateString project, TemplateString condition)
        {
            this.Project = project;
            this.Condition = condition;
        }

        public TemplateString Condition { get; }
        public TemplateString Project { get; }

        public void AddTo(XmlDocument doc, XmlElement root)
        {
            var elem = doc.AddElement(root, nameof(Import));
            doc.AddAttributeWithValue(elem, nameof(Project), Project.ToString());
            if (!string.IsNullOrEmpty(Condition.ToString()))
            {
                doc.AddAttributeWithValue(elem, nameof(Condition), Condition.ToString());
            }
        }

        public ITemplate Substitute(string key, string value)
        {
            return new Import(Project.Substitute(key, value), Condition.Substitute(key, value));
        }
    }
}
