﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Xml;

namespace CSProjBuilder
{
    /// <summary>
    /// Property impl, supports tempalting
    /// </summary>
    public class Property : ITemplate
    {
        public Property(string key, string value, string condition)
            : this(new ResolvedTemplateString(key), new ResolvedTemplateString(value), new ResolvedTemplateString(condition))
        {
        }

        public Property(TemplateString key, TemplateString value, TemplateString condition)
        {
            Name = key;
            Value = value;
            Condition = condition;
        }

        public TemplateString Name { get; }
        public TemplateString Value { get; }
        public TemplateString Condition { get; }

        public ITemplate Substitute(string key, string value)
        {
            return new Property(Name.Substitute(key, value), Value.Substitute(key, value), Condition.Substitute(key, value));
        }
    }


    /// <summary>
    /// Property Group impl, supports templating
    /// </summary>
    public class PropertyGroup : IAddToXml, ITemplate
    {
        public PropertyGroup(IList<Property> properties, string condition) : this(properties, new ResolvedTemplateString(condition))
        {
        }

        public PropertyGroup(IList<Property> properties, TemplateString condition)
        {
            Condition = condition;
            Properties = properties.ToImmutableList();
        }

        public ImmutableList<Property> Properties { get; }
        public TemplateString Condition { get; }

        public void AddTo(XmlDocument doc, XmlElement root)
        {
            var group = doc.AddElement(root, "PropertyGroup");
            if (!string.IsNullOrEmpty(Condition?.ToString()))
            {
                doc.AddConditionAttribute(group, Condition.ToString());
            }
            foreach (var property in Properties)
            {
                var prop = doc.AddElement(group, property.Name.ToString());
                doc.AddConditionAttribute(prop, property.Condition.ToString());
                prop.InnerText = property.Value.ToString();
            }
        }

        public ITemplate Substitute(string key, string value)
        {
            return new PropertyGroup(Properties.Select(p => (Property)p.Substitute(key, value)).ToList(), Condition?.Substitute(key, value));
        }
    }
}