﻿using System;
using System.Collections.Immutable;

namespace CSProjBuilder
{
    /// <summary>
    /// IPropertyGropuBuilder impl
    /// </summary>
    internal class PropertyGroupBuilder : IPropertyGroupBuilder
    {
        private readonly ProjFile parent;
        private readonly ImmutableList<Property> properties;
        private readonly TemplateString condition;

        public PropertyGroupBuilder(ProjFile projFile)
        {
            this.parent = projFile;
            properties = ImmutableList.Create<Property>();
        }

        private PropertyGroupBuilder(ProjFile projFile, ImmutableList<Property> properties, TemplateString condition)
        {
            this.parent = projFile;
            this.properties = properties;
            this.condition = condition;
        }

        public ProjFile EndPropertyGroup()
        {
            return this.parent.WithPropertyGroup(this.ToPropertyGroup());
        }

        private PropertyGroup ToPropertyGroup()
        {
            return new PropertyGroup(properties, condition);
        }

        public IPropertyGroupBuilder WithCondition(string condition)
        {
            return this.WithCondition(new ResolvedTemplateString(condition));
        }

        public IPropertyGroupBuilder WithConditionalProperty(string property, string value, string condition)
        {
            return this.WithConditionalProperty(new ResolvedTemplateString(property), new ResolvedTemplateString(value), new ResolvedTemplateString(condition));
        }

        public IPropertyGroupBuilder WithProperty(string property, string value)
        {
            return this.WithProperty(new ResolvedTemplateString(property), new ResolvedTemplateString(value));
        }

        public IPropertyGroupBuilder WithConditionalProperty(TemplateString property, TemplateString value, TemplateString condition)
        {
            var prop = new Property(property, value, condition);
            return new PropertyGroupBuilder(parent, properties.Add(prop), this.condition);
        }

        public IPropertyGroupBuilder WithProperty(TemplateString property, TemplateString value)
        {
            return this.WithConditionalProperty(property, value, new TemplateString(null));
        }

        public IPropertyGroupBuilder WithCondition(TemplateString condition)
        {
            return new PropertyGroupBuilder(parent, properties, condition);
        }
    }
}