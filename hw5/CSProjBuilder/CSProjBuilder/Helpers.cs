﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSProjBuilder
{
    /// <summary>
    /// Helpers that make it easier to build up Reference/Compile/None/ProjectReference 
    /// elements. If you want to make a templatable element, simply call the overload
    /// that takes a TemplateString with your desired template name. Make sure to 
    /// substitute the templates in the project file before you serialize it!
    /// </summary>
    public class ElementHelpers
    {
        public static Element Reference(TemplateString reference)
        {
            return new Element("Reference").WithAttribute(new ResolvedTemplateString("Include"), reference);
        }

        public static Element Reference(string reference)
        {
            return Reference(new ResolvedTemplateString(reference));
        }

        public static Element Compile(TemplateString compile)
        {
            return new Element("Compile").WithAttribute(new ResolvedTemplateString("Include"), compile);
        }

        public static Element Compile(string compile)
        {
            return Compile(new ResolvedTemplateString(compile));
        }

        public static Element None(TemplateString none)
        {
            return new Element("None").WithAttribute(new ResolvedTemplateString("Include"), none);
        }

        public static Element None(string none)
        {
            return None(new ResolvedTemplateString(none));
        }

        public static Element ProjectReference(string projectPath, string projectName, string projectGuid)
        {
            var elem = new Element("ProjectReference").WithAttribute("Includ", projectPath);
            elem = elem.WithNestedElement(new Element("Project", projectGuid));
            return elem.WithNestedElement(new Element("Name", projectName));
        }
    }
}
