﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CSProjBuilder
{
    /// <summary>
    /// Represents a generic XML Element (like under ItemGroups). Can have nested elements, a value, and attributes.
    /// Use the template string overloads to for templating. 
    /// You can pass in a "ResolvedTemplateString" for parameters that shouldn't be templated.
    /// </summary>
    public class Element : ITemplate, IAddToXml
    {
        private readonly ImmutableList<KeyValuePair<TemplateString, TemplateString>> _attributes;
        private readonly ImmutableList<Element> _nestedElements;

        public Element(string name) : this(new ResolvedTemplateString(name), new ResolvedTemplateString(""))
        {
        }

        public Element(string name, string value) : this(new ResolvedTemplateString(name), new ResolvedTemplateString(value))
        {
        }

        public Element(TemplateString name, TemplateString value) : this(name, value, ImmutableList.Create<KeyValuePair<TemplateString, TemplateString>>(), ImmutableList.Create<Element>())
        {
        }

        private Element(TemplateString name, TemplateString value, ImmutableList<KeyValuePair<TemplateString, TemplateString>> attributes, ImmutableList<Element> nestedElements)
        {
            Name = name;
            Value = value;
            _attributes = attributes;
            _nestedElements = nestedElements;
        }

        public IReadOnlyList<KeyValuePair<TemplateString, TemplateString>> Attributes => _attributes;
        public TemplateString Name { get; }
        public TemplateString Value { get; }

        public void AddTo(XmlDocument doc, XmlElement target)
        {
            var thisElem = doc.AddElement(target, Name.ToString());
            if (!string.IsNullOrEmpty(Value.ToString()))
            {
                thisElem.InnerText = Value.ToString();
            }
            foreach (var attr in _attributes)
            {
                doc.AddAttributeWithValue(thisElem, attr.Key.ToString(), attr.Value.ToString());
            }

            foreach (var nested in _nestedElements)
            {
                nested.AddTo(doc, thisElem);
            }
        }

        public ITemplate Substitute(string key, string value)
        {
            var elem = new Element(Name.Substitute(key, value), Value.Substitute(key, value));
            foreach (var attr in _attributes)
            {
                elem = elem.WithAttribute(attr.Key.Substitute(key, value), attr.Value.Substitute(key, value));
            }

            foreach (var nested in _nestedElements)
            {
                elem = elem.WithNestedElement((Element)nested.Substitute(key, value));
            }

            return elem;
        }

        public Element WithAttribute(string key, string value)
        {
            return this.WithAttribute(new ResolvedTemplateString(key), new ResolvedTemplateString(value));
        }

        public Element WithAttribute(TemplateString key, TemplateString value)
        {
            return new Element(Name, Value, _attributes.Add(new KeyValuePair<TemplateString, TemplateString>(key, value)), _nestedElements);
        }

        public Element WithNestedElement(Element nested)
        {
            return new Element(Name, Value, _attributes, _nestedElements.Add(nested));
        }
    }
}
