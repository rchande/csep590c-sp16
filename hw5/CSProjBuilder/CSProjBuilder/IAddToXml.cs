﻿using System.Xml;

namespace CSProjBuilder
{
    /// <summary>
    /// Internal interface used for serialization
    /// </summary>
    internal interface IAddToXml
    {
        void AddTo(XmlDocument doc, XmlElement target);
    }
}