﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CSProjBuilder
{
    /// <summary>
    /// Internal internal extensions that make adding XmlElements to an XMlDocument easier.
    /// (Used for serialization)
    /// </summary>
    internal static class Extensions
    {
        public static XmlElement AddElement(this XmlDocument doc, XmlElement target, string name)
        {
            var elem = doc.CreateElement(name);
            target.AppendChild(elem);
            return elem;
        }

        public static XmlAttribute AddAttributeWithValue(this XmlDocument doc, XmlElement target, string name, string value)
        {
            var attr = doc.CreateAttribute(name);
            attr.Value = value;
            target.Attributes.Append(attr);
            return attr;
        }

        public static XmlAttribute TryAddAttributeWithValue(this XmlDocument doc, XmlElement target, string name, string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return doc.AddAttributeWithValue(target, name, value);
            }

            return null;
        }

        public static XmlAttribute AddConditionAttribute(this XmlDocument doc, XmlElement target, string conditionText)
        {
            return doc.TryAddAttributeWithValue(target, "Condition", conditionText);
        }
    }
}
