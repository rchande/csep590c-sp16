﻿namespace CSProjBuilder
{
    /// <summary>
    /// Implemented by types that support templating.
    /// Call Substitute with key and value on an item
    /// and it will recursively attempt to replace any templates
    /// based on key with value.
    /// </summary>
    public interface ITemplate
    {
        ITemplate Substitute(string key, string value);
    }
}