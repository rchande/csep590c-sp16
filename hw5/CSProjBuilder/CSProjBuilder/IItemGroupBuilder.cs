﻿namespace CSProjBuilder
{
    /// <summary>
    /// Exposed by the API after ProjFile.WithItemGroup() is called
    /// Allows fluent addition of elements to the item group.
    /// </summary>
    public interface IItemGroupBuilder
    {
        /// <summary>
        /// Return the project file with the item group added
        /// </summary>
        /// <returns></returns>
        ProjFile EndItemGroup();
        /// <summary>
        /// Adds a condition to the item group
        /// <summary>
        IItemGroupBuilder WithCondition(string condition);
        /// <summary>
        /// Returns the ItemGroupBuilder with this element added to it.
        /// </summary>
        IItemGroupBuilder WithItem(Element elem);
    }
}