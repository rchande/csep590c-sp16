﻿using System;
using System.Collections.Immutable;
using System.Linq;
using System.Xml;

namespace CSProjBuilder
{
    /// <summary>
    /// Implementation of ItemGroup
    /// </summary>
    public class ItemGroup : IAddToXml, ITemplate
    {
        private ImmutableList<Element> elements;

        public ItemGroup(ImmutableList<Element> elements)
        {
            this.elements = elements;
        }

        public void AddTo(XmlDocument doc, XmlElement root)
        {
            var group = doc.AddElement(root, "ItemGroup");
            foreach (var element in elements)
            {
                element.AddTo(doc, group);
            }
        }

        public ITemplate Substitute(string key, string value)
        {
            return new ItemGroup(elements.Select(e => (Element)e.Substitute(key, value)).ToImmutableList());
        }
    }
}