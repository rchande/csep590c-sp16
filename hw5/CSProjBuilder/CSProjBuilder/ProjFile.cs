﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace CSProjBuilder
{
    /// <summary>
    /// The class used by users to build up a project file.
    /// There are methods for adding attributes to a project file as well as nested elements.
    /// You can add PropertyGroups, ItemGroups, and Import elements for now.
    /// The project file will track the order that items are added and emit them in that order.
    /// If you construct any elements with TemplateStrings, you will need to call `Substitute` to
    /// replace each string before calling ToString to get a project file.
    /// This model is fully immutable.
    /// </summary>
    public class ProjFile
    {
        private readonly string _initialTargets;
        private readonly string _defaultTargets;
        private readonly string _toolsVersion;
        private readonly string _xmlns;

        private readonly ImmutableDictionary<string, string> _additionalAttrs;
        private readonly ImmutableList<IAddToXml> _queue;

        public string InitialTargets => _initialTargets;
        public string DefaultTargets => _defaultTargets;
        public string ToolsVersion => _toolsVersion;
        public string Xmlns => _xmlns;

        public IReadOnlyDictionary<string, string> AdditionalAttributes => _additionalAttrs;
        public IReadOnlyList<PropertyGroup> PropertyGroups => _queue.OfType<PropertyGroup>().ToImmutableList();
        public IReadOnlyList<Import> Imports => _queue.OfType<Import>().ToImmutableList();


        private ProjFile(string initialTargets, string defaultTargets, string toolsVersion, string xmlns, ImmutableDictionary<string, string> attrs, ImmutableList<IAddToXml> queue)
        {
            this._initialTargets = initialTargets;
            this._defaultTargets = defaultTargets;
            this._toolsVersion = toolsVersion;
            this._xmlns = xmlns;
            this._additionalAttrs = attrs;
            this._queue = queue;
        }

        public ProjFile()
        {
            this._xmlns = "http://schemas.microsoft.com/developer/msbuild/2003";
            this._toolsVersion = "14.0";
            _queue = ImmutableList.Create<IAddToXml>();
            _additionalAttrs = ImmutableDictionary.Create<string, string>();
        }

        public ProjFile WithInitialTargets(string initialTargets)
        {
            return new ProjFile(initialTargets, _defaultTargets, _toolsVersion, _xmlns, _additionalAttrs, _queue);
        }

        public ProjFile WithDefaultTargets(string defaultTargets)
        {
            return new ProjFile(_initialTargets, defaultTargets, _toolsVersion, _xmlns, _additionalAttrs, _queue);
        }

        public ProjFile WithToolsVersion(string toolsVersion)
        {
            return new ProjFile(_initialTargets, _defaultTargets, toolsVersion, _xmlns, _additionalAttrs, _queue);
        }

        public ProjFile WithInitialXMLns(string xmlns)
        {
            return new ProjFile(_initialTargets, _defaultTargets, _toolsVersion, xmlns, _additionalAttrs, _queue);
        }

        public ProjFile WithPropertyGroup(PropertyGroup propertyGroup)
        {
            var newQueue = _queue.Add(propertyGroup);
            return new ProjFile(_initialTargets, _defaultTargets, _toolsVersion, _xmlns, _additionalAttrs, newQueue);
        }

        public ProjFile WithItemGroup(ItemGroup itemGroup)
        {
            var newQueue = _queue.Add(itemGroup);
            return new ProjFile(_initialTargets, _defaultTargets, _toolsVersion, _xmlns, _additionalAttrs, newQueue);
        }

        public ProjFile WithAdditionalAttribute(string key, string value)
        {
            if (_additionalAttrs.ContainsKey(key))
            {
                throw new ArgumentException(nameof(key));
            }

            return new ProjFile(_initialTargets, _defaultTargets, _toolsVersion, _xmlns, _additionalAttrs.Add(key, value), _queue);
        }

        public ProjFile ReplaceAdditionalAttribute(string key, string value)
        {
            if (!_additionalAttrs.ContainsKey(key))
            {
                throw new ArgumentException(nameof(key));
            }

            var d = _additionalAttrs.Remove(key);
            return new ProjFile(_initialTargets, _defaultTargets, _toolsVersion, _xmlns, d.Add(key, value), _queue);
        }

        public IPropertyGroupBuilder WithPropertyGroup()
        {
            return new PropertyGroupBuilder(this);
        }

        public IItemGroupBuilder WithItemGroup()
        {
            return new ItemGroupBuilder(this);
        }

        public ProjFile WithImport(string projectPath)
        {
            var import = new Import(projectPath, null);
            return this.WithImport(import);
        }

        public ProjFile WithConditionalImport(string projectPath, string condition)
        {
            var import = new Import(projectPath, condition);
            return this.WithImport(import);
        }

        private ProjFile WithImport(Import import)
        {
            var newQueue = _queue.Add(import);
            return new ProjFile(_initialTargets, _defaultTargets, _toolsVersion, _xmlns, _additionalAttrs, newQueue);
        }

        public override string ToString()
        {
            var doc = new XmlDocument();
            var root = doc.CreateElement("Project");
            doc.TryAddAttributeWithValue(root, nameof(InitialTargets), InitialTargets);
            doc.TryAddAttributeWithValue(root, nameof(ToolsVersion), ToolsVersion);
            doc.TryAddAttributeWithValue(root, nameof(DefaultTargets), DefaultTargets);
            doc.TryAddAttributeWithValue(root, "xmlns", Xmlns);
            doc.AppendChild(root);

            foreach (var attr in _additionalAttrs)
            {
                var at = doc.CreateAttribute(attr.Key);
                at.Value = attr.Value;
                root.Attributes.Append(at);
            }

            foreach (var item in _queue)
            {
                item.AddTo(doc, root);
            }

            var result = XDocument.Parse(doc.InnerXml).ToString();
            return result;
        }

        public ProjFile Substitute(string key, string value)
        {
            var newQueue = new List<IAddToXml>();
            foreach (ITemplate item in _queue)
            {
                newQueue.Add((IAddToXml)item.Substitute(key, value));
            }

            return new ProjFile(_initialTargets, _defaultTargets, _toolsVersion, _xmlns, _additionalAttrs, newQueue.ToImmutableList());
        }
    }
}