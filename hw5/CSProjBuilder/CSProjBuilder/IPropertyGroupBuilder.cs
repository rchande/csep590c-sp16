﻿namespace CSProjBuilder
{
    /// <summary>
    ///  Interface exposed after calling ProjectFile.AddPropertyGroup()
    ///  Supports templating
    /// </summary>
    public interface IPropertyGroupBuilder
    {
        /// <summary>
        /// Adds a property with a condition
        /// </summary>
        IPropertyGroupBuilder WithConditionalProperty(string property, string value, string condition);

        /// <summary>
        /// Adds a property with a condition
        /// </summary>
        IPropertyGroupBuilder WithConditionalProperty(TemplateString property, TemplateString value, TemplateString condition);

        /// <summary>
        /// Adds an unconditional property
        /// </summary>
        /// <param name="property"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        IPropertyGroupBuilder WithProperty(string property, string value);

        /// <summary>
        /// Adds an unconditional property
        /// </summary>
        /// <param name="property"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        IPropertyGroupBuilder WithProperty(TemplateString property, TemplateString value);

        /// <summary>
        /// Adds a condition to the property group
        /// </summary>
        /// <returns></returns>
        IPropertyGroupBuilder WithCondition(string condition);

        /// <summary>
        /// Adds a condition to the property group
        /// </summary>
        /// <returns></returns>
        IPropertyGroupBuilder WithCondition(TemplateString condition);

        /// <summary>
        ///  Completes the property group and returns the containing project element for editing
        /// </summary>
        /// <returns></returns>
        ProjFile EndPropertyGroup();
    }
}