﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CSProjBuilder
{
    /// <summary>
    /// A type that assists in the initial conversion of a csproj to code that generates a csproj.
    /// This tool does not generate the simplest possible program that would generate this csproj and may 
    /// not properly handle nested items in ItemGroups (particularly project references).
    /// </summary>
    public class Converter
    {
        private const string toolsVersionName = "ToolsVersion";
        private const string defaultTargetsName = "DefaultTargets";
        private const string xmlnsName = "xmlns";
        private const string initialTargetsName = "InitialTargets";
        private static readonly string[] WellKnownProjectAttributes = new[] { toolsVersionName, defaultTargetsName, xmlnsName, initialTargetsName };

        public static string ConvertCSProjToProgram(string csproj)
        {
            var doc = new XmlDocument();
            doc.LoadXml(csproj);

            return Visit(doc.DocumentElement);
        }

        private static string Visit(XmlElement documentElement)
        {
            string program = "var proj = new ProjFile();";
            if (documentElement.Name == "Project")
            {
                var proj = new ProjFile();
                if (documentElement.HasAttribute(toolsVersionName))
                    program = AppendProjAssignment(program, $"proj.WithToolsVersion(@\"{documentElement.GetAttribute(toolsVersionName)}\")");

                if (documentElement.HasAttribute(defaultTargetsName))
                    program = AppendProjAssignment(program, $"proj.WithDefaultTargets(@\"{documentElement.GetAttribute(defaultTargetsName)}\")");

                if (documentElement.HasAttribute(xmlnsName))
                    program = AppendProjAssignment(program, $"proj.WithInitialXMLns(@\"{documentElement.GetAttribute(xmlnsName)}\")");

                if (documentElement.HasAttribute(initialTargetsName))
                    program = AppendProjAssignment(program, $"proj.WithInitialTargets(@\"{documentElement.GetAttribute(initialTargetsName)}\")");

                foreach (var attribute in documentElement.Attributes)
                {
                    var attr = attribute as XmlAttribute;
                    if (!WellKnownProjectAttributes.Contains(attr.Name))
                    {
                        AppendProjAssignment(program, $"proj.WithAdditionalAttribute(@\"{attr.Name}\", @\"{attr.Value})\"");
                        proj = proj.WithAdditionalAttribute(attr.Name, attr.Value);
                    }
                }

                foreach (var child in documentElement.ChildNodes)
                {
                    program += VisitChild(child as XmlElement);
                }
            }

            return program;
        }

        private static string VisitChild(XmlElement xmlElement)
        {
            var stub = "";
            if (xmlElement == null)
            {
                // Unhandled
                return "";
            }

            if (xmlElement.Name == "Import")
            {
                var project = xmlElement.GetAttribute("Project");
                var condition = xmlElement.GetAttribute("Condition");
                return AppendProjAssignment(stub, $"proj.WithConditionalImport(@\"{project}\", @\"{condition}\")");
            }

            if (xmlElement.Name == "PropertyGroup")
            {
                var condition = xmlElement.GetAttribute("Condition");
                stub = $"proj = proj.WithPropertyGroup()\r\n.WithCondition($@\"{condition}\")\r\n";
                foreach (XmlElement elem in xmlElement.ChildNodes)
                {
                    var name = elem.Name;
                    var value = elem.InnerText;
                    condition = elem.GetAttribute("Condition");

                    stub += $".WithConditionalProperty(@\"{name}\",@\"{value}\",@\"{condition}\")\r\n";
                }
                stub += ".EndPropertyGroup();\r\n";
                return stub;
            }

            if (xmlElement.Name == "ItemGroup")
            {
                var condition = xmlElement.GetAttribute("Condition");
                stub = $"proj = proj.WithItemGroup()\r\n.WithCondition($@\"{condition}\")\r\n";
                foreach (XmlElement elem in xmlElement.ChildNodes)
                {
                    var name = elem.Name;

                    stub += $".WithItem(new Element(@\"{name}\")";

                    foreach (XmlAttribute attribute in elem.Attributes)
                    {
                        stub += $".WithAttribute(@\"{attribute.Name}\", @\"{attribute.InnerText}\")";
                    }

                    stub += ")\r\n";
                }
                stub += ".EndItemGroup();\r\n";
            }

            // Currently unhandled
            return stub;
        }

        private static string AppendProjAssignment(string existing, string expression)
        {
            return $"{existing}\r\nproj = {expression};";
        }

    }
}
