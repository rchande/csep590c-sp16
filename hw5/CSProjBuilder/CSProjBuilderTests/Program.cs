﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSProjBuilder;

namespace CSProjBuilderTests
{
    class Program
    {
        static void Main(string[] args)
        {
            Test0();
            Test1();
            Test2();
            TestReferencesAndOrdering();
            TestTemplating();
            TestTemplating2();
        }


        private static void Test0()
        {
            var text = @"<Project ToolsVersion=""14.0"" DefaultTargets=""Build"" xmlns=""http://schemas.microsoft.com/developer/msbuild/2003"">
</Project>";

            var expected = @"var proj = new ProjFile();
proj = proj.WithToolsVersion(@""14.0"");
proj = proj.WithDefaultTargets(@""Build"");
proj = proj.WithInitialXMLns(@""http://schemas.microsoft.com/developer/msbuild/2003"");";

            var result = Converter.ConvertCSProjToProgram(text);
            Util.AssertEqual(result, expected);

            var proj = new ProjFile();
            proj = proj.WithToolsVersion("14.0");
            proj = proj.WithDefaultTargets("Build");
            proj = proj.WithInitialXMLns("http://schemas.microsoft.com/developer/msbuild/2003");

            // Linq renders this as a single line element
            var projStr = proj.ToString();
            //Util.AssertEqual(projStr, text);
        }

        private static void Test1()
        {
            var text = @"<Project ToolsVersion=""14.0"" DefaultTargets=""Build"" xmlns=""http://schemas.microsoft.com/developer/msbuild/2003"">
  <Import Project=""$(MSBuildExtensionsPath)\$(MSBuildToolsVersion)\Microsoft.Common.props"" Condition=""Exists('$(MSBuildExtensionsPath)\$(MSBuildToolsVersion)\Microsoft.Common.props')"" />
  <Import Project=""$(MSBuildToolsPath)\Microsoft.CSharp.targets"" />
</Project>";

            var result = CSProjBuilder.Converter.ConvertCSProjToProgram(text);
            var expected = @"var proj = new ProjFile();
proj = proj.WithToolsVersion(@""14.0"");
proj = proj.WithDefaultTargets(@""Build"");
proj = proj.WithInitialXMLns(@""http://schemas.microsoft.com/developer/msbuild/2003"");
proj = proj.WithConditionalImport(@""$(MSBuildExtensionsPath)\$(MSBuildToolsVersion)\Microsoft.Common.props"", @""Exists('$(MSBuildExtensionsPath)\$(MSBuildToolsVersion)\Microsoft.Common.props')"");
proj = proj.WithConditionalImport(@""$(MSBuildToolsPath)\Microsoft.CSharp.targets"", @"""");";
            Util.AssertEqual(result, expected);

            var proj = new ProjFile();
            proj = proj.WithToolsVersion("14.0");
            proj = proj.WithDefaultTargets("Build");
            proj = proj.WithInitialXMLns("http://schemas.microsoft.com/developer/msbuild/2003");
            proj = proj.WithConditionalImport(@"$(MSBuildExtensionsPath)\$(MSBuildToolsVersion)\Microsoft.Common.props", @"Exists('$(MSBuildExtensionsPath)\$(MSBuildToolsVersion)\Microsoft.Common.props')");
            proj = proj.WithConditionalImport(@"$(MSBuildToolsPath)\Microsoft.CSharp.targets", "");

            var projStr = proj.ToString();
            Util.AssertEqual(projStr, text);
        }

        private static void Test2()
        {
            var text = @"<Project ToolsVersion=""14.0"" DefaultTargets=""Build"" xmlns=""http://schemas.microsoft.com/developer/msbuild/2003"">
  <PropertyGroup>
    <Configuration Condition="" '$(Configuration)' == '' "">Debug</Configuration>
    <ProjectGuid>{C97E2B61-7B21-42E1-A00D-E8294464AEA3}</ProjectGuid>
  </PropertyGroup>
  <PropertyGroup Condition="" '$(Configuration)|$(Platform)' == 'Debug|AnyCPU' "">
    <DebugSymbols>true</DebugSymbols>
    <DebugType>full</DebugType>
  </PropertyGroup>
</Project>";

            var expected = @"var proj = new ProjFile();
proj = proj.WithToolsVersion(@""14.0"");
proj = proj.WithDefaultTargets(@""Build"");
proj = proj.WithInitialXMLns(@""http://schemas.microsoft.com/developer/msbuild/2003"");proj = proj.WithPropertyGroup()
.WithCondition($@"""")
.WithConditionalProperty(@""Configuration"",@""Debug"",@"" '$(Configuration)' == '' "")
.WithConditionalProperty(@""ProjectGuid"",@""{C97E2B61-7B21-42E1-A00D-E8294464AEA3}"",@"""")
.EndPropertyGroup();
proj = proj.WithPropertyGroup()
.WithCondition($@"" '$(Configuration)|$(Platform)' == 'Debug|AnyCPU' "")
.WithConditionalProperty(@""DebugSymbols"",@""true"",@"""")
.WithConditionalProperty(@""DebugType"",@""full"",@"""")
.EndPropertyGroup();
";

            var result = Converter.ConvertCSProjToProgram(text);
            Util.AssertEqual(result, expected);

            var proj = new ProjFile();
            proj = proj.WithToolsVersion(@"14.0");
            proj = proj.WithDefaultTargets(@"Build");
            proj = proj.WithInitialXMLns(@"http://schemas.microsoft.com/developer/msbuild/2003");
            proj = proj.WithPropertyGroup()
             .WithConditionalProperty(@"Configuration", @"Debug", @" '$(Configuration)' == '' ")
             .WithProperty(@"ProjectGuid", @"{C97E2B61-7B21-42E1-A00D-E8294464AEA3}")
             .EndPropertyGroup();
            proj = proj.WithPropertyGroup()
            .WithCondition($@" '$(Configuration)|$(Platform)' == 'Debug|AnyCPU' ")
            .WithProperty(@"DebugSymbols", @"true")
            .WithProperty(@"DebugType", @"full")
            .EndPropertyGroup();

            Util.AssertEqual(proj.ToString(), text);
        }

        private static void TestReferencesAndOrdering()
        {
            var text = @"<Project ToolsVersion=""14.0"" DefaultTargets=""Build"" xmlns=""http://schemas.microsoft.com/developer/msbuild/2003"">
  <PropertyGroup>
    <Configuration Condition="" '$(Configuration)' == '' "">Debug</Configuration>
    <ProjectGuid>{C97E2B61-7B21-42E1-A00D-E8294464AEA3}</ProjectGuid>
  </PropertyGroup>
  <ItemGroup>
    <Reference Include=""System"" />
    <Reference Include=""System.Xml"" />
  </ItemGroup>
  <PropertyGroup Condition="" '$(Configuration)|$(Platform)' == 'Debug|AnyCPU' "">
    <DebugSymbols>true</DebugSymbols>
    <DebugType>full</DebugType>
  </PropertyGroup>
  <ItemGroup>
    <Compile Include=""Class1.cs"" />
  </ItemGroup>
</Project>";

            var expected = @"var proj = new ProjFile();
proj = proj.WithToolsVersion(@""14.0"");
proj = proj.WithDefaultTargets(@""Build"");
proj = proj.WithInitialXMLns(@""http://schemas.microsoft.com/developer/msbuild/2003"");proj = proj.WithPropertyGroup()
.WithCondition($@"""")
.WithConditionalProperty(@""Configuration"",@""Debug"",@"" '$(Configuration)' == '' "")
.WithConditionalProperty(@""ProjectGuid"",@""{C97E2B61-7B21-42E1-A00D-E8294464AEA3}"",@"""")
.EndPropertyGroup();
proj = proj.WithItemGroup()
.WithCondition($@"""")
.WithItem(new Element(@""Reference"").WithAttribute(@""Include"", @""System""))
.WithItem(new Element(@""Reference"").WithAttribute(@""Include"", @""System.Xml""))
.EndItemGroup();
proj = proj.WithPropertyGroup()
.WithCondition($@"" '$(Configuration)|$(Platform)' == 'Debug|AnyCPU' "")
.WithConditionalProperty(@""DebugSymbols"",@""true"",@"""")
.WithConditionalProperty(@""DebugType"",@""full"",@"""")
.EndPropertyGroup();
proj = proj.WithItemGroup()
.WithCondition($@"""")
.WithItem(new Element(@""Compile"").WithAttribute(@""Include"", @""Class1.cs""))
.EndItemGroup();
";

            var result = Converter.ConvertCSProjToProgram(text);
            Util.AssertEqual(result, expected);

            var proj = new ProjFile();
            proj = proj.WithToolsVersion(@"14.0");
            proj = proj.WithDefaultTargets(@"Build");
            proj = proj.WithInitialXMLns(@"http://schemas.microsoft.com/developer/msbuild/2003"); proj = proj.WithPropertyGroup()
             .WithCondition($@"")
             .WithConditionalProperty(@"Configuration", @"Debug", @" '$(Configuration)' == '' ")
             .WithConditionalProperty(@"ProjectGuid", @"{C97E2B61-7B21-42E1-A00D-E8294464AEA3}", @"")
             .EndPropertyGroup();
            proj = proj.WithItemGroup()
            .WithCondition($@"")
            .WithItem(new Element(@"Reference").WithAttribute(@"Include", @"System"))
            .WithItem(new Element(@"Reference").WithAttribute(@"Include", @"System.Xml"))
            .EndItemGroup();
            proj = proj.WithPropertyGroup()
            .WithCondition($@" '$(Configuration)|$(Platform)' == 'Debug|AnyCPU' ")
            .WithConditionalProperty(@"DebugSymbols", @"true", @"")
            .WithConditionalProperty(@"DebugType", @"full", @"")
            .EndPropertyGroup();
            proj = proj.WithItemGroup()
            .WithCondition($@"")
            .WithItem(new Element(@"Compile").WithAttribute(@"Include", @"Class1.cs"))
            .EndItemGroup();

            Util.AssertEqual(proj.ToString(), text);
        }

        private static void TestTemplating()
        {
            var text = @"<Project ToolsVersion=""14.0"" DefaultTargets=""Build"" xmlns=""http://schemas.microsoft.com/developer/msbuild/2003"">
  <PropertyGroup>
    <Configuration Condition=""Condition1"">Debug</Configuration>
    <ProjectGuid>{C97E2B61-7B21-42E1-A00D-E8294464AEA3}</ProjectGuid>
  </PropertyGroup>
</Project>";

            var proj = new ProjFile();
            proj = proj.WithToolsVersion(@"14.0");
            proj = proj.WithDefaultTargets(@"Build");
            proj = proj.WithInitialXMLns(@"http://schemas.microsoft.com/developer/msbuild/2003"); proj = proj.WithPropertyGroup()
             .WithCondition($@"")
             .WithConditionalProperty(new ResolvedTemplateString(@"Configuration"), new ResolvedTemplateString(@"Debug"), new TemplateString("$Condition1"))
             .WithConditionalProperty(@"ProjectGuid", @"{C97E2B61-7B21-42E1-A00D-E8294464AEA3}", @"")
             .EndPropertyGroup();

            proj = proj.Substitute("$Condition1", "Condition1");

            var result = proj.ToString();
            Util.AssertEqual(result, text);
        }

        private static void TestTemplating2()
        {
            var text = @"<Project ToolsVersion=""14.0"" DefaultTargets=""Build"" xmlns=""http://schemas.microsoft.com/developer/msbuild/2003"">
  <PropertyGroup>
    <Configuration Condition=""Condition1"">Debug</Configuration>
    <ProjectGuid>{C97E2B61-7B21-42E1-A00D-E8294464AEA3}</ProjectGuid>
  </PropertyGroup>
  <ItemGroup>
    <Reference Include=""Reference1"" />
    <Reference Include=""System.Xml"" />
  </ItemGroup>
  <PropertyGroup Condition=""Condition2"">
    <DebugSymbols>true</DebugSymbols>
    <DebugType>full</DebugType>
  </PropertyGroup>
  <ItemGroup>
    <Compile Include=""Class1.cs"" />
  </ItemGroup>
</Project>";

            var proj = new ProjFile();
            proj = proj.WithToolsVersion(@"14.0");
            proj = proj.WithDefaultTargets(@"Build");
            proj = proj.WithInitialXMLns(@"http://schemas.microsoft.com/developer/msbuild/2003"); proj = proj.WithPropertyGroup()
             .WithCondition($@"")
             .WithConditionalProperty(new ResolvedTemplateString(@"Configuration"), new ResolvedTemplateString( @"Debug"), new TemplateString("$Condition1"))
             .WithConditionalProperty(@"ProjectGuid", @"{C97E2B61-7B21-42E1-A00D-E8294464AEA3}", @"")
             .EndPropertyGroup();
            proj = proj.WithItemGroup()
            .WithCondition($@"")
            .WithItem(new Element(@"Reference").WithAttribute(new ResolvedTemplateString(@"Include"), new TemplateString(@"$Reference")))
            .WithItem(new Element(@"Reference").WithAttribute(@"Include", @"System.Xml"))
            .EndItemGroup();
            proj = proj.WithPropertyGroup()
            .WithCondition(new TemplateString("$Condition2"))
            .WithConditionalProperty(@"DebugSymbols", @"true", @"")
            .WithConditionalProperty(@"DebugType", @"full", @"")
            .EndPropertyGroup();
            proj = proj.WithItemGroup()
            .WithCondition($@"")
            .WithItem(new Element(@"Compile").WithAttribute(@"Include", @"Class1.cs"))
            .EndItemGroup();

            proj = proj.Substitute("$Condition1", "Condition1")
                        .Substitute("$Condition2", "Condition2")
                        .Substitute("$Reference", "Reference1");

            var result = proj.ToString();
            Util.AssertEqual(result, text);
        }

    }
}
